package com.carlos.flappybird;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Preferences;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Circle;
import com.badlogic.gdx.math.Intersector;
import com.badlogic.gdx.math.Rectangle;

import java.util.Random;

public class FlappyBird extends ApplicationAdapter {
	private SpriteBatch batch;
	private Texture fundo;
	private Texture[] passaro;
	private Texture canoInferior;
	private Texture canoSuperior;
	private float variacaoYCanos;
	private float espacoEntreCanos;
	private float movmentoCano;
	private float posicaoCanoHorizontal;
	private Random random;
	private BitmapFont pontuacao;
	private BitmapFont textoReiniciar;
	private BitmapFont melhorPontuação;
	private Texture gameOver;

	private Sound sonVoando;
	private Sound sonColisao;
	private Sound sonPontuação;

	//Formas para colisão
	private ShapeRenderer shapeRenderer;
	private Circle circuloPassaro;
	private Rectangle retanguloCanoCima;
	private Rectangle retanguloCanoBaixo;

	private int estadoDoJogo;


	float x = 0;
	float y = 0;
	private float larguraDispositivo;
	private float alturaDispositivo;

	private int contador = 0;private int contador2 = 0;
	private float gravidade = 0;
	private int i = 0;
	private int pontos;
	private int pontosMaximo;
	private int contador3;
	private boolean primeiraVez = true;

	private Preferences preferences;
	@Override
	public void create () {
	   inicializaTexturas();
	   iniicializaObjetos();
	}

	@Override
	public void render () {
		desenhandoNaTela();
        dectarColisoes();
		estados();
		validaPontos();

	}
	@Override
	public void dispose () {
		batch.dispose();
        passaro[0].dispose();
        passaro[1].dispose();
        passaro[2].dispose();
	}

	private void desenhandoNaTela(){
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		
		batch.begin();
		batch.draw(fundo,0,0,larguraDispositivo,alturaDispositivo);
		batch.draw(canoInferior,posicaoCanoHorizontal,alturaDispositivo/2-canoInferior.getHeight()-espacoEntreCanos/2+variacaoYCanos/2);
		batch.draw(canoSuperior,posicaoCanoHorizontal,alturaDispositivo/2 + espacoEntreCanos/2+variacaoYCanos/2);
		batch.draw(passaro[i], x, y);
		pontuacao.draw(batch,String.valueOf(pontos),larguraDispositivo/2,alturaDispositivo-30);
        if(estadoDoJogo == 2){
            batch.draw(gameOver,larguraDispositivo/2-gameOver.getWidth()/2,alturaDispositivo/2);
            textoReiniciar.draw(batch,"Toque para reiniciar",larguraDispositivo/2 - 150,alturaDispositivo/2 - gameOver.getHeight()/2  );
			melhorPontuação.draw(batch,"Sua maior potuação foi: "+pontosMaximo+" pontos",larguraDispositivo/2-200 ,alturaDispositivo/2-gameOver.getHeight());
        }
		batch.end();



	}

	private void estados(){
		contador++;
		gravidade++;



		if(contador == 10){
			i = 1;
		}else if(contador == 15){
			i = 2;
		}else if(contador == 25){
			i = 0;
			contador =0;
		}

		if(estadoDoJogo == 0){
			//Esperando iniício
			if(Gdx.input.justTouched()){
				gravidade = -15;
				estadoDoJogo = 1;
				sonVoando.play();
			}
		}else if(estadoDoJogo == 1){
			//Jogando sem colisão
			y = y - gravidade;
			posicaoCanoHorizontal = posicaoCanoHorizontal-movmentoCano;
			if(Gdx.input.justTouched()){
				//Evento de clique "pulo do passaro"
				gravidade = -15;
				sonVoando.play();
			}
			if(posicaoCanoHorizontal < -1* canoInferior.getWidth()){
				//Coloca canos de volta na posição inicial com altura randomizada
				posicaoCanoHorizontal = larguraDispositivo;
				variacaoYCanos = random.nextFloat()*alturaDispositivo/2 - alturaDispositivo/4;

			}
			if(y<0 || y> alturaDispositivo + 100){
				//Verifica se passaro saio da tela, caso sim da game over
				estadoDoJogo = 2;
			}

		}else if(estadoDoJogo == 2){
			// Game Over, Mostra a pontuação e sava a nova pontuação máxia
			if(pontos > pontosMaximo ){
				preferences.putInteger("pontuacaoMaxima",pontos);
				preferences.flush();
				pontosMaximo = pontos;
			}
			y = y - gravidade;
			x = x - 3;

			if(Gdx.input.justTouched()){
				//Quando ocorre toque o jogo reinicia
                create();
			}
		}
	}


	private void inicializaTexturas(){
		fundo = new Texture("fundo.png");
		passaro = new Texture[3];
		passaro[0] = new Texture("passaro1.png");
		passaro[1] = new Texture("passaro2.png");
		passaro[2] = new Texture("passaro3.png");
		canoInferior = new Texture("cano_baixo_maior.png");
		canoSuperior = new Texture("cano_topo_maior.png");
		gameOver = new Texture("game_over.png");
	}

	private void iniicializaObjetos(){
		larguraDispositivo = Gdx.graphics.getWidth();
		alturaDispositivo = Gdx.graphics.getHeight();
		batch = new SpriteBatch();
		y = alturaDispositivo/2;
		espacoEntreCanos = 193;
		movmentoCano = 5;
		posicaoCanoHorizontal = larguraDispositivo;
		random = new Random();
		variacaoYCanos = random.nextFloat()*alturaDispositivo/2;
		contador2 =0;
		pontuacao = new BitmapFont();
		pontuacao.setColor(Color.RED);
		pontuacao.getData().setScale(5);
		retanguloCanoBaixo = new Rectangle();
		retanguloCanoCima = new Rectangle();
		circuloPassaro = new Circle();
		shapeRenderer = new ShapeRenderer();
		estadoDoJogo = 0;
		textoReiniciar = new BitmapFont();
		textoReiniciar.setColor(Color.RED);
		textoReiniciar.getData().setScale(2);
		melhorPontuação = new BitmapFont();
		melhorPontuação.setColor(Color.BLACK);
		melhorPontuação.getData().setScale(2);
		pontos = 0;

		sonColisao = Gdx.audio.newSound(Gdx.files.internal("som_batida.wav"));
        sonVoando = Gdx.audio.newSound(Gdx.files.internal("som_asa.wav"));
        sonPontuação = Gdx.audio.newSound(Gdx.files.internal("som_pontos.wav"));
		x = 50;

		//pontosMaximo = 0;
		preferences = Gdx.app.getPreferences("recorde");
		pontosMaximo = preferences.getInteger("pontuacaoMaxima",0);
		if(primeiraVez){
			contador3 = 0;
			primeiraVez = false;
		}

	}

	public void validaPontos(){
		if(posicaoCanoHorizontal == 40){
			pontos++;
			sonPontuação.play();
		}
	}

	public void dectarColisoes(){


		circuloPassaro.set(50+passaro[0].getWidth()/2,y+passaro[0].getHeight()/2,passaro[0].getHeight()/2);
		retanguloCanoBaixo.set(posicaoCanoHorizontal,alturaDispositivo/2-canoInferior.getHeight()-espacoEntreCanos/2+variacaoYCanos/2,canoInferior.getWidth(),canoInferior.getHeight());
		retanguloCanoCima.set(posicaoCanoHorizontal,alturaDispositivo/2 + espacoEntreCanos/2+variacaoYCanos/2,canoInferior.getWidth(),canoInferior.getHeight());

		if((Intersector.overlaps(circuloPassaro,retanguloCanoBaixo)||Intersector.overlaps(circuloPassaro,retanguloCanoCima)) && estadoDoJogo == 1){
			//pontos = 1000;

			sonColisao.play();
			contador3++;
			if(contador3 == -4){
				estadoDoJogo = 3;
			}else {
				estadoDoJogo = 2;
			}
		}

	}
}
